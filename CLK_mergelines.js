/*
    MERGE LINES
    - for sang & polos (staff 1 & 2, voice 1),
      merge into jublag staff (staff 3, voice 3), shaded
    - merging 2 lines into 1 might require 3 cursors, moving separately
    - probably simpler to copy lines to different voices on same staff,
      then remove extra rests, etc. to look nice

    NOTES
    - MuseScore 2.x has a new plugin framework
    - see removenotes.js and removenotes.ui for Qt user interface
*/

var menu = "Plugins.CLK.Merge Lines";
// init plugin when first loaded (program startup)
function init() { print( menu + ".init" ); };

// return object with start&end staff&tick for current selection
function getSelection(cursor) {
    cursor.goToSelectionEnd  ();
    var endStaff    = cursor.staff;
    var endTick     = cursor.tick();
    cursor.goToSelectionStart();
    if ( cursor.eos() ) { // endofscore, so no selection, so select all
        endStaff = curScore.staves;
        cursor.rewind();
    }
    var startStaff  = cursor.staff;
    var startTick   = cursor.tick();
//    print("staff start=" + startStaff + " end=" + endStaff);
//    print("tick  start=" + startTick  + " end=" + endTick );
    return { startStaff:startStaff, startTick:startTick, endStaff:endStaff, endTick:endTick };
}

var mergeList   = [ { fromStaff:0, fromVoice:0, toStaff:2, toVoice:2 },
                    { fromStaff:1, fromVoice:0, toStaff:2, toVoice:3 } ]

function mergeLines() {
    print( menu );
    var cursor      = new Cursor(curScore);
    var sel         = getSelection(cursor);
    var noteCount   = 0;

    cursor.rewind();
    while (cursor.tick() < sel.endTick) {
        for (var i=0; i<fromList.length; i++) {
            var from = fromList[i];
            cursor.staff = from.staff;
            cursor.voice = from.voice;
        }
        cursor.next();
    }
    print( menu + "=" + noteCount );
}; // end mergeLines

// close plugin when unloaded
function close() { print( menu + "close" ); };

var mscorePlugin = {
   majorVersion:  1,
   minorVersion:  1,
   menu   : menu,
   init   : init,
   run    : mergeLines,
   onClose: close
};

mscorePlugin;
