MuseScore 1.x plugins
- (c) Copyright 2014-2016 by Clif Kussmaul, clif@kussmaul.org

- master versions in GitLab and in ~\Data\projects\music
- copy to C:\Program Files*\MuseScore\plugins
- NOTE: written for MuseScore 1.x, now out of date

CLK_labelnotes.js 
- maps midi notes to labels to add labels for each note
- created to add gamelan note numbers
- could also be used to add note names (A, A#, etc)

CLK_mergelines.js
- merges 2 lines into one
- created to merge sang & polos (staff 1&2, voice 1), into jublag (staff 3, voice 3), shaded;
  this shows the actual melody from interlocking parts

