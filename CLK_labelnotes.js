/*
    LABEL NOTES
    - maps midi note (0-127) to labels in array miditolabel
    - see examples for letters (A-G), gamelan notes (1-10)

    NOTES
    - MuseScore 2.x has a new plugin framework
    - see removenotes.js and removenotes.ui for Qt user interface
*/

// TODO: find range of note positions to adjust xoff/yoff
// TODO: ? detect/use key signature for sharps & flats
// FUTURE: remove note labels - not in 1.x plugin architecture
// - workaround - - select one, select all similar, delete
// FUTURE: customize labels via GUI (letters, midi notes, solfage etc)

var menu            = "Plugins.CLK.Label Notes";

//var miditolabel     = [ "C","C#","D","D#","E","F","F#","G","G#","A","A#","B" ];
//                        C   D   E     G   A     C    D   E     G   A
//var miditolabel     = [ 5,0,6,0,7,0,0,8,0,9,0,0,10,0,1,0,2,0,0,3,0,4,0,0 ];
//                          C#D   E       G#A        C#D   E       G#A
var miditolabel       = [ 0,1,2,0,3,0,0,0,5,6,0,0, 0,1,2,0,3,0,0,0,5,6,0,0 ];

// label XY offsets from center of staff - should be float, but framework parses as ints
//  -2=just above top line; 4=just below bottom line
var labelXOff       = [ 0, 0, 0];
//var labelYOff       = [ 4, 4,-2]; // labels inside system - S&P (S1&S2) below staff, J&G (S3) above staff
//var labelYOff       = [-5,-5,-3]; // labels above staff
var labelYOff       = [4,4,5]; // labels below staff


// init plugin when first loaded (program startup)
function init() { print( menu + ".init" ); };

// return object with start&end staff&tick for current selection
function getSelection(cursor) {
    cursor.goToSelectionEnd  ();
    var endStaff    = cursor.staff;
    var endTick     = cursor.tick();
    cursor.goToSelectionStart();
    if ( cursor.eos() ) { // endofscore, so no selection, so select all
        endStaff = curScore.staves;
        cursor.rewind();
    }
    var startStaff  = cursor.staff;
    var startTick   = cursor.tick();
//    print("staff start=" + startStaff + " end=" + endStaff);
//    print("tick  start=" + startTick  + " end=" + endTick );
    return { startStaff:startStaff, startTick:startTick, endStaff:endStaff, endTick:endTick };
}

function Queue(size) {
    this.d    = new Array(size);
    // add new value, remove oldest value
    this.add  = function(v) { if (this.d[0] != v) { this.d.unshift(v); return this.d.pop(); } }
    // does queue have given value?
    this.has  = function(v) { return ( -1 != this.d.indexOf(v) );     }
    // clear
    this.clr  = function()  { this.d = new Array(this.d.length);      }
}

function labelNotes() {
    print( menu );
    var cursor      = new Cursor(curScore);
    var sel         = getSelection(cursor);
    var noteCount   = 0;

    // CAN'T USE cursor.staff++ cursor.voice++
    for ( var staff=sel.startStaff; staff<sel.endStaff; staff++ ) {
        for ( var voice=0; voice<4; voice++ ) { // always 4 voices
            var queue = new Queue(2);
            cursor.goToSelectionStart();
            if (cursor.eos()) cursor.rewind();
            // must set staff and voice _after_ cursor
            cursor.staff = staff;
            cursor.voice = voice;
            print( "staff=" + staff + " voice=" + voice );
            while ( cursor.tick() < sel.endTick ) {
                if ( cursor.isChord() ) {
                    var pitch = cursor.chord().topNote().pitch;
                    if ( ! queue.has( pitch ) ) {
                        labelNote(curScore, cursor);
                    }
                    queue.add(pitch);
                    noteCount += cursor.chord().notes;
                }
                cursor.next();
            }
        }
    }
    print(menu + "=" + noteCount);
}; // end labelNotes

function labelNote(score, cursor) {
    var t         = new Text(score);
    t.defaultFont = new QFont("Times",12);
//    t.text        = cursor.chord().topNote().name;
    t.text        = miditolabel [ cursor.chord().topNote().pitch % miditolabel.length ];
    t.xOffset     = labelXOff   [ cursor.staff ];
    t.yOffset     = labelYOff   [ cursor.staff ];
    cursor.putStaffText(t);
    return cursor.chord().topNote().pitch;
};

// close plugin when unloaded
function close() { print(menu + ".close"); };

var mscorePlugin = {
   majorVersion:  1,
   minorVersion:  1,
   menu   : menu,
   init   : init,
   run    : labelNotes,
   onClose: close
};

mscorePlugin;
